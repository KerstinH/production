# Migrate large projects off file-XX-stor-gprd to file-YY-stor-gprd

## What?

Migrate the large projects currently on `file-XX-stor-gprd.c.gitlab-production.internal` (https://dashboards.gitlab.net/d/W_Pbu9Smk/storage-stats?orgId=1&refresh=30m&fullscreen&panelId=144) to `file-YY-stor-gprd.c.gitlab-production.internal` (https://dashboards.gitlab.net/d/W_Pbu9Smk/storage-stats?orgId=1&refresh=30m&fullscreen&panelId=150)

## Why?

Because git data file server disk usage is high: `80.92%` full.

We need to build new file servers or rebalance repos.

Since we have four other file storage nodes with more than 50% capacity available, a rebalance instead of additional node creation, seems optimal.


## Procedure

- [ ] Production change requires commented manager approval.

/cc @dawsmith

### Setup

- [ ] Install the [rebalance script](https://gitlab.com/gitlab-com/runbooks/blob/master/scripts/storage_rebalance.rb) on a common system (such as the production console system host).

```{sh}
ssh console-01-sv-gprd.c.gitlab-production.internal
tmux attach || tmux  # attach to an existing tmux session or create a new one
sudo su - root
mkdir -p /var/opt/gitlab/scripts/logs
chown -R git:git /var/opt/gitlab/scripts
cd /var/opt/gitlab/scripts
curl --location --silent --remote-name https://gitlab.com/gitlab-com/runbooks/raw/master/scripts/storage_rebalance.rb
chown -R git:git /var/opt/gitlab/scripts/storage_rebalance.rb
chmod +x /var/opt/gitlab/scripts/storage_rebalance.rb
gitlab-rails runner /var/opt/gitlab/scripts/storage_rebalance.rb --help

```

- [ ] Export a personal access token in an environment variable on the console system host.

```{sh}
export PRIVATE_TOKEN=CHANGEME
```


### Dry-run

- [ ] Execute a dry-run of the rebalance script to move files from `nfs-fileXX` to `nfs-fileYY`.

Do not give an amount of disk space to migrate (`--move-amount N`), so that only one single project will be migrated for the time being.

```{sh}
gitlab-rails runner /var/opt/gitlab/scripts/storage_rebalance.rb --verbose --current-file-server=nfs-fileXX --target-file-server=nfs-fileYY --wait=10800 --dry-run=yes
```

- [ ] Verify that the dry-run execution behaved as expected (no errors).


### Don't jump in the deep end

- [ ] Execute the rebalance script to move files from `nfs-fileXX` to `nfs-fileYY`.

Do not give an amount of disk space to migrate (`--move-amount N`), so that only one single project will be migrated for the time being.  Specify that the rebalance script will wait much longer than 10 seconds, to ensure that the script continues to monitor the progress of the project migration until it has completed.

```{sh}
gitlab-rails runner /var/opt/gitlab/scripts/storage_rebalance.rb --dry-run=no --verbose --current-file-server=nfs-fileXX --target-file-server=nfs-fileYY --wait=10800 | tee /var/opt/gitlab/scripts/logs/nfs-fileXX.migration.$(date +%Y-%m-%d_%H%M).log
```

- [ ] Verify that the execution behaved as expected (no errors).


### Wade in

- [ ] Execute the rebalance script to move files from `nfs-fileXX` to `nfs-fileYY`.

Specify an amount of disk space in gigabytes (less than `16000`) to migrate.

```{sh}
gitlab-rails runner /var/opt/gitlab/scripts/storage_rebalance.rb --dry-run=no --verbose --current-file-server=nfs-fileXX --target-file-server=nfs-fileYY --wait=10800 --move-amount=600 | tee nfs-fileXX.migration.$(date +%Y-%m-%d_%H%M).log
```

- [ ] Verify that the execution behaved as expected (no errors).

### Clean up

- [ ] Install the [cleanup script](https://gitlab.com/gitlab-com/runbooks/blob/master/scripts/storage_cleanup.rb) on a common system (such as the production console system host).

```{sh}
ssh console-01-sv-gprd.c.gitlab-production.internal
cd /tmp
curl --location --silent --remote-name https://gitlab.com/gitlab-com/runbooks/raw/master/scripts/storage_cleanup.rb
sudo mv /tmp/storage_cleanup.rb /var/opt/gitlab/scripts/
sudo chown -R git:git /var/opt/gitlab/scripts/storage_cleanup.rb
sudo chmod +x /var/opt/gitlab/scripts/storage_cleanup.rb
/var/opt/gitlab/scripts/storage_cleanup.rb --help
```

- [ ] Perform a dry-run clean-up the `moved` projects remaining on the source storage node host system.

```{sh}
/var/opt/gitlab/scripts/storage_cleanup.rb --dry-run=yes --verbose --node=file-XX-stor-gprd.c.gitlab-production.internal
```

Ensure that there were no errors from the execution of this script.

- [ ] Take a snapshot of the disk for `nfs-fileXX`.

Navigate to https://console.cloud.google.com/compute/disksDetail/zones/us-east1-c/disks/file-XX-stor-gprd-data?project=gitlab-production in a browser. Click the "Create Snapshot" button, and then click the "Create" button. Or the following commands may be used in a shell session on your local workstation:

```{sh}
gcloud auth login
gcloud config set project gitlab-production
gcloud config set compute/region us-east1
gcloud config set compute/zone us-east1-c
gcloud compute disks list | grep file-XX-stor-gprd-data
gcloud compute disks snapshot file-XX-stor-gprd-data
```

- [ ] Finally clean-up the `moved` projects remaining on the source storage node host system.

```{sh}
/var/opt/gitlab/scripts/storage_cleanup.rb --dry-run=no --verbose --node=file-XX-stor-gprd.c.gitlab-production.internal
```

/label ~infrastructure ~C3 ~change ~toil ~"Service:Gitaly" ~"requires production access"
