<!--
Please review https://about.gitlab.com/handbook/engineering/infrastructure/change-management/ for the most recent information on our change plans and exection policies.
-->

# Production Change - Criticality 1 ~"C1"

| Change Objective                                       | Describe the objective of the change                                                                                                               |
|:-------------------------------------------------------|:---------------------------------------------------------------------------------------------------------------------------------------------------|
| Change Type                                            | ConfigurationChange\|HotFix\|DeploymentNewFeature\|Operation                                                                                       |
| Services Impacted                                      | List services                                                                                                                                      |
| Change Team Members                                    | Name of the engineers involved in the change                                                                                                       |
| Change Severity                                        | C1                                                                                                                                                 |
| Change Reviewer                                        | A colleague who will review the change                                                                                                             |
| Tested in staging                                      | Evidence or assertion the change was tested on staging environment                                                                                 |
| Dry-run output                                         | If the change is done through a script, it is mandatory to have a dry-run capability in the script, run the change in dry-run mode and output the result |
| Due Date                                               | Date and time in UTC timezone for the execution of the change, if possible add the local timezone of the engineer executing the change             |
| Time tracking                                          | To estimate and record times associated with changes ( including a possible rollback )                                                             |
| Downtime Component                                     | if yes how many minutes                                                                                                                            |

## Detailed steps for the change

<!--
For each step the following must be considered:
  * pre-conditions for execution of the step - how to verify it is safe to proceed
  * execution commands for the step - what to do
  * post-execution validation for the step - how to verify the step succeeded

It is strongly recommended to:
  * Note relevant graphs in grafana to monitor the effect of the change, including how to identify that it has worked, or has caused undue negative effects
  * Review alerts that may go off that can be silenced pro-actively
-->

## Rollback steps

<!--
  * As for the original steps.  It may be acceptable to reference the change steps as the process, with variations (e.g. Revert commit and run deployment).
  * It is acceptable to list a full rollback process, and allow for the applier to select where to start based on how far through they got.
-->

## Changes checklist

<!--
Before commencing work, inform the person on-call at minimum.
To find out who is on-call, in #production channel run: /chatops run oncall production.
-->

- [ ] Detailed steps and rollback steps have been filled prior to commencing work
- [ ] Person on-call has been informed prior to change being rolled out

/label ~"C1" ~"change::unscheduled"
