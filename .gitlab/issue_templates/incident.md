<!---
**Please note:** if the incident relates to sensitive data, or is security
related consider labeling this issue with ~security and mark it confidential.

Please enter a title of the form "YYYY-MM-DD: briefly describe problem".
-->

## Summary

<!--
Leave a brief headline remark so that people know what's going on. It's fine for
this to be vague while not much is known.
-->

More information will be added as we investigate the issue.

## Timeline

All times UTC.

YYYY-MM-DD

- 00:00 - ...

<!--- Don't forget to add labels for severity (S1 - S4) and service. -->

## Resources

1. If the **Situation Zoom room** was utilised, recording will be automatically uploaded to [Incident room Google Drive folder](https://drive.google.com/drive/folders/1wtGTU10-sybbCv1LiHIj2AFEbxizlcks) (private)


/label ~incident
